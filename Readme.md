# lsWebScrape v0.1-Alpha
A C++ wrapper for gumbo C library.

## Functions
``` c++
string plain_text( const string& url );
vector<string> find_links( const string& url );
string get_domain( const string& url );
string get_robots( const string& url );
```

## plain_text
Extracts all plain text from a page.

## find_links
Builds a list of all internal links on a page.

## get_domain
Extract the domain of any URL.

## get_robots
Download the robots.txt file of any URL.

# TODO
* Parse robots.txt
