#include "lsWebScrape.h"

int main()
{
	lsWebScrape scrape;
	string url( "https://en.wikipedia.org/wiki/Aliens_(film)" );
	
	// page contents
	cout << "### Page : " << url << endl;
	string result = scrape.plain_text( url );
	cout << "### Result : " << result << endl;
	
	// page links
	cout << "### Links." << endl;
	vector<string> links = scrape.find_links( url );
	for( const string& link : links )
	{
		cout << "Link : " << link << endl;
	}
	
	return 0;
}
