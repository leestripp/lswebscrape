#include "lsWebScrape.h"

int main()
{
	lsWebScrape scrape;
	string url, domain;
	
	cout << endl << "### Extract domains -------------------------" << endl;
	url = "https://en.wikipedia.org/wiki/Aliens_(film)";
	cout << "Url    : " << url << endl;
	cout << "domain : " << scrape.get_domain( url ) << endl;
	
	url = "http://en.wikipedia.org/wiki/Aliens_(film)";
	cout << "Url    : " << url << endl;
	cout << "domain : " << scrape.get_domain( url ) << endl;
	
	url = "duckduckgo.com/?t=h_&q=Aliens+movie&ia=web";
	cout << "Url    : " << url << endl;
	cout << "domain : " << scrape.get_domain( url ) << endl;
	
	// get_robots (robots.txt)
	url = "https://en.wikipedia.org/wiki/Aliens_(film)";
	cout << endl << "### -----------------------------------------" << endl;
	cout << "Get robots.txt from : " << url << endl;
	cout << "robots.txt" << endl;
	cout << scrape.get_robots( url );
	
	return 0;
}

