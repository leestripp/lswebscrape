#ifndef LSWEBSCRAPE_H
#define LSWEBSCRAPE_H

// C
#include <gumbo.h>

// C++
#include <iostream>
#include <sstream>
#include <vector>

// lsCurl
#include <lsCurl/lsCurl.h>

using namespace std;

class lsWebScrape
{
public:
	lsWebScrape();
	
	string plain_text( const string& url );
	vector<string> find_links( const string& url );
	string get_domain( const string& url );
	string get_robots( const string& url );
	
private:
	string cleantext( GumboNode* node );
	void search_for_links( GumboNode* node );
	// utils
	bool is_splitter( const char& ch );
	
	lsCurl m_curl;
	vector<string> m_links;
	vector<char> m_splitter_list;
};

#endif // LSWEBSCRAPE_H
