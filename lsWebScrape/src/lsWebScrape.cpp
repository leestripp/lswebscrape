#include "lsWebScrape.h"

lsWebScrape::lsWebScrape()
{
	// Split URL's by:
	m_splitter_list.push_back( '/' );
	m_splitter_list.push_back( '?' );
}

string lsWebScrape::plain_text( const string& url )
{
	m_curl.m_mode = LSM_GET;
	m_curl.m_url = url;
	m_curl.start();
	// wait.
	m_curl.join();
	
	if( m_curl.get_pageBuffer().empty() )
	{
		return string();
	}
	
	GumboOutput* output = gumbo_parse( m_curl.get_pageBuffer().c_str() );
	string result( cleantext(output->root) );
	gumbo_destroy_output( &kGumboDefaultOptions, output );
	
	return result;
}

vector<string> lsWebScrape::find_links( const string& url )
{
	// clear old links.
	m_links.clear();
	
	m_curl.m_mode = LSM_GET;
	m_curl.m_url = url;
	m_curl.start();
	// wait.
	m_curl.join();
	
	if( m_curl.get_pageBuffer().empty() )
	{
		// return empty result.
		return m_links;
	}
	
	GumboOutput* output = gumbo_parse( m_curl.get_pageBuffer().c_str() );
	search_for_links( output->root );
	gumbo_destroy_output( &kGumboDefaultOptions, output );
	
	return m_links;
}

// Private

string lsWebScrape::cleantext( GumboNode* node )
{
	if( node->type == GUMBO_NODE_TEXT )
	{
		return string( node->v.text.text );
		
	} else if( node->type == GUMBO_NODE_ELEMENT && node->v.element.tag != GUMBO_TAG_SCRIPT && node->v.element.tag != GUMBO_TAG_STYLE )
	{
		string contents = "";
		GumboVector* children = &node->v.element.children;
		for( uint i=0; i<children->length; i++ )
		{
			const string text = cleantext( (GumboNode*)children->data[i] );
			if( i != 0 && !text.empty() )
			{
				contents.append( " " );
			}
			contents.append( text );
		}
		return contents;
	}
	
	return "";
}

// internal links.

void lsWebScrape::search_for_links( GumboNode* node )
{
	if( node->type != GUMBO_NODE_ELEMENT )
	{
		return;
	}
	
	GumboAttribute* href;
	if( node->v.element.tag == GUMBO_TAG_A && (href = gumbo_get_attribute(&node->v.element.attributes, "href")) )
	{
		string tmp( href->value );
		if( tmp.substr(0, 4) != "http" )
		{
			m_links.push_back( string(href->value) );
		}
		// debug
		// cout << href->value << endl;
	}
	
	GumboVector* children = &node->v.element.children;
	for( uint i=0; i<children->length; i++ )
	{
		search_for_links( (GumboNode*)children->data[i] );
	}
}

// utils

string lsWebScrape::get_robots( const string& url )
{
	string domain = get_domain( url );
	if( domain.empty() )
	{
		cerr << "ERROR: invalid domain." << endl;
		return domain;
	}
	
	m_curl.m_mode = LSM_GET;
	m_curl.m_url = "https://" + domain + "/robots.txt";
	m_curl.start();
	// wait.
	m_curl.join();
	
	if( m_curl.get_pageBuffer().empty() )
	{
		return "N/A";
	}
	
	return m_curl.get_pageBuffer();
}

string lsWebScrape::get_domain( const string& url )
{
	string domain;
	
	// split url into parts.
	vector<string> parts;
	stringstream ss;
	for( ulong i=0; i<url.size(); i++ )
	{
		if( is_splitter( url[i] ) )
		{
			if( ss.rdbuf()->in_avail() != 0 )
			{
				parts.push_back( ss.str() );
				ss.str( string() );
			}
		} else
		{
			ss << url[i];
		}
	}
	// grab last part.
	if( ss.rdbuf()->in_avail() != 0 )
	{
		parts.push_back( ss.str() );
	}
	
	/* debug
	int i=0;
	for( const string& item : parts )
	{
		cout << i << " : " << item << endl;
		i++;
	}
	*/
	
	if( parts[0] == "http:" || parts[0] == "https:" )
	{
		domain = parts[1];
	} else
	{
		domain = parts[0];
	}
	
	return domain;
}

bool lsWebScrape::is_splitter( const char& ch )
{
	for( const char& c : m_splitter_list )
	{
		if( ch == c ) return true;
	}
	return false;
}

